//No.1 (Range)
console.log("Nomor 1");
function range(a="",b=""){
    if(a=="" || b==""){
        return -1;
    }else{
        value = [];
        if(a<b){
            for(i=a;i<=b;i++){
                value.push(i);
            }
        }else{
            for(i=a;i>=b;i--){
                value.push(i);
            }
        }
        return value;
    }
}
console.log(range(1,10));
console.log(range(1));
console.log(range(11,18));
console.log(range(54,50));
console.log(range());
console.log("\n");


//No.2 (Range With Steps)
console.log("Nomor 2");
function rangeWithStep(startNum,finishNum,step){
    value = [];
    if(startNum<finishNum){
        for(i=startNum;i<=finishNum;i = i+step){
            value.push(i);
        }
    }else{
        for(i=startNum;i>=finishNum;i = i-step){
            value.push(i);
        }
    }
    return value;
}
console.log(rangeWithStep(1,10,2));
console.log(rangeWithStep(11,23,3));
console.log(rangeWithStep(5,2,1));
console.log(rangeWithStep(29,2,4));
console.log("\n");


console.log("Nomor 3");
//No.3 (Sum of Range)
function sum(startNum="",finishNum="",step=1){
    rangeWithStep(startNum,finishNum,step)
    hasil = 0;
    for(i=0;i<value.length;i++){
        hasil+=value[i];
    }
    return hasil;
}
console.log(sum(1,10));
console.log(sum(5,50,2));
console.log(sum(15,10));
console.log(sum(20,10,2));
console.log(sum(1));
console.log(sum());
console.log("\n");

//No.4 (Array Multidimensi)
console.log("Nomor 4");
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] ;

function dataHandling(data){
    for(i=0;i<input.length;i++){
        console.log("Nomor ID : " + data[i][0]);
        console.log("Nama Lengkap : " + data[i][1]);
        console.log("TTL : " + data[i][2]+" "+data[i][3]);
        console.log("Hobi : " + data[i][4]);
        console.log("\n");
    }
}
dataHandling(input);
console.log("\n");


//No.5 (Balik Kata);
console.log("Nomor 5");
function balikKata(kata){
    hasil = "";
    for(i=kata.length-1;i>=0;i--){
        hasil += kata[i];
    }
    return hasil;
}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 
console.log("\n");


//No.6 (Metode Array)
console.log("Nomor 6");
input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]  ;
function dataHandling2(data){
    var newArray = data.slice();
    newArray.splice(4,0,"Pria", "SMA Internasional Metro");
    newArray = newArray.slice(0,6);
    newArray[1] = data[1]+" Elsharawy";
    newArray[2] = "Provinsi "+data[2]
    console.log(newArray);

    date = newArray[3];
    date = date.split("/");
    switch(date[1]){
        case "01" : {console.log("Januari ");break;}
        case "02" : {console.log("Februari");break;}
        case "03" : {console.log("Maret");break;}
        case "04" : {console.log("April");break;}
        case "05" : {console.log("Mei");break;}
        case "06" : {console.log("Juni");break;}
        case "07" : {console.log("Juli");break;}
        case "08" : {console.log("Agustus");break;}
        case "09" : {console.log("September");break;}
        case "10" : {console.log("Oktober");break;}
        case "11" : {console.log("November");break;}
        case "12" : {console.log("Desember");break;}
    }
    
    console.log(date.sort(function (a,b){return b-a}));
    date = newArray[3];
    date = date.split("/");
    date = date.join("-");
    console.log(date);
    console.log(newArray[1].slice(0,14))

}
dataHandling2(input);