//No.1 (Array to Object)
console.log("No.1 (Array to Object)");
function arrayToObject(arr){
    var now = new Date()
    var thisYear = now.getFullYear();
    var panjang = arr.length;

    if(panjang>0){
        for(var i=0;i<panjang;i++){
            if(arr[i][3]>thisYear || arr[i][3] == null || arr[i][3] == ""){
                arr[i][3] = "Invalid Birth Year";
            }else{
                arr[i][3] = thisYear - arr[i][3];
            }
            peopleObj = Object.assign({},people[i]);
            arr[i] = peopleObj;
            
            peopleObj["firstName"] = peopleObj["0"];
            peopleObj["lastName"] = peopleObj["1"];
            peopleObj["gender"] = peopleObj["2"];
            peopleObj["age"] = peopleObj["3"];
            delete peopleObj["0"];
            delete peopleObj["1"];
            delete peopleObj["2"];
            delete peopleObj["3"];
        }
        var namaArr = [];
        for(i=0;i<arr.length;i++){
            nama = arr[i]["firstName"]+" "+arr[i]["lastName"];
            namaArr.push(nama);
        }
        arr = Object.assign({},arr);
        
        for(i=0;i<panjang;i++){
            arr[namaArr[i]] = arr[i];
            delete arr[i];
        }
        return arr;
    }else{
        return "";
    }
}
var people = [["Abduh", "Muhamad", "male", 1992], ["Ahmad", "Taufik", "male", 1985],["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"],["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]];
console.log(arrayToObject(people));
console.log("\n");



//No.2 (Shopping Time)
console.log("No.2 (Shopping Time)");
function shoppingTime(memberId,money){
    var listHarga = {
    'Sepatu Stacattu' : 1500000,
    'Baju Zoro' : 500000,
    'Baju H&N' : 250000,
    'Sweater Uniklooh' : 175000,
    'Casing Handphone' : 50000
    };

    if(memberId == null || memberId==""){
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    }else if(money<50000){
        return "Mohon maaf, uang tidak cukup";
    }else{
        var sisa = money;
        var totalHarga = 0;
        var itemArr = [];
        for(item in listHarga){
            if(sisa<listHarga[item]){
                continue;
            }else if(sisa<0){
                break;
            }else{
                itemArr.push(item);
                sisa = sisa-listHarga[item];
                totalHarga += listHarga[item];
            }
        var kembali = money - totalHarga;
        }
        var hasil = {};
        hasil.memberId = memberId;
        hasil.money = money;
        hasil.listPurchased = itemArr;
        hasil.changeMoney = kembali;

        return hasil;
    }
}
//console.log(shoppingTime("324193hDew2",700000));
console.log(shoppingTime("324193hDew2",700000))
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja
console.log("\n");



//No.3 (Naik Angkot)
console.log("No.3 (Naik Angkot)");
function naikAngkot(listPenumpang){
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    newArr = []
    item = {}
    bayar = 0;
    for(i=0;i<listPenumpang.length;i++){
        item.penumpang = listPenumpang[i][0];
        item.naikDari = listPenumpang[i][1];
        item.Tujuan = listPenumpang[i][2];
        bayar = (rute.indexOf(item.Tujuan) - rute.indexOf(item.naikDari))*2000
        item.bayar = bayar;
        newArr.push(item)
    }
    return newArr;
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));