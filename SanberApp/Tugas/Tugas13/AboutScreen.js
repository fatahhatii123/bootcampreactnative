import React from 'react';
import {Platform, View, Text, StyleSheet, Image, TouchableOpacity, FlatList, ViewPagerAndroidComponent, ImageStore, ColorPropType, TextInput, Button} from 'react-native';
import {Input, Item, Content} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import { MaterialCommunityIcons, FontAwesome5 } from '@expo/vector-icons'; 

export default class App extends React.Component{

    render(){
        return(
            <View style={styles.container}>

                <View style={styles.profile}>
                    <Text style={styles.title}>Tentang Saya</Text>
                    <Image source={require('./images/498852.png')} style={styles.profileImage} />
                    <Text style={[styles.title,{marginTop:25}]}>Achmad Hilmy</Text>
                    <Text style={[styles.title,{marginTop:10}]}>React Native Developer</Text>
                </View>

                <View style={styles.section}>
                    <Text style={styles.header}>Portofolio</Text>
                    <View style={styles.items}>
                        <TouchableOpacity style={styles.tabItem}>
                            <Icon name="gitlab" style={{alignItems:"center", justifyContent:"center", marginHorizontal:30}} size={30} color="black" />
                            <Text style={styles.tabTitle}>@Sanbercode</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.tabItem}>
                            <Icon name="github" style={{alignItems:"center", justifyContent:"center", marginHorizontal:30}} size={30} color="black" />
                            <Text style={styles.tabTitle}>@Sanbercode</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.section}>
                    <Text style={styles.header}>Contact</Text>
                    <View style={styles.items}>
                            <TouchableOpacity style={styles.tabItem}>
                                <MaterialCommunityIcons name="linkedin" style={{alignItems:"center", justifyContent:"center", marginHorizontal:30}} size={30} color="black" />
                                <Text style={styles.tabTitle}>@Sanbercode</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.tabItem}>
                                <FontAwesome5 name="whatsapp-square" style={{alignItems:"center", justifyContent:"center", marginHorizontal:28}} size={30} color="black" />
                                <Text style={styles.tabTitle}>0818023142</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.tabItem}>
                                <MaterialCommunityIcons name="gmail" style={{alignItems:"center", justifyContent:"center", marginHorizontal:30}} size={30} color="black" />
                                <Text style={styles.tabTitle}>@Sanbercode</Text>
                            </TouchableOpacity>
                        </View>
                </View>
            </View>
            
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        marginTop: 20
    },
    title:{
        marginTop: 70,
        color: "#000",
        fontSize: 24,
        fontWeight: "bold",
        justifyContent: 'center',
        textAlign: 'center'
    },
    profile: {
        backgroundColor: 'white',
        flexDirection: 'column',
        alignItems : 'center',
    },
    profileImage : {
        marginTop: 40,
        resizeMode: "stretch",
        height: 150,
        width: 150,
        alignItems: 'center',
        justifyContent: 'center'
    },
    section: {
        backgroundColor: "gray",
        marginHorizontal: "10%",
        marginTop: "8%",
        paddingHorizontal: 15,
        paddingVertical: 10
    },
    header : {
        fontWeight: "bold",
        fontSize: 19,
        borderBottomWidth: 1,
        marginBottom: 15,
    },
    items: {
        height:60,
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    tabTitle: {
        fontSize: 15,
        paddingTop: 4
    }
})