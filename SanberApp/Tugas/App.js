import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';


import YoutubeUI from './Tugas/Tugas12/App';
import LoginScreen from './Tugas/Tugas13/LoginScreen';
import RegisterScreen from './Tugas/Tugas13/RegisterScreen';
import AboutScreen from './Tugas/Tugas13/AboutScreen';
import TodoApp from './Tugas/Tugas14/App'

export default function App() {
  return (
    //<YoutubeUI />
    //<LoginScreen />
    //<RegisterScreen />
    // <AboutScreen />
    <TodoApp />
  )
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
