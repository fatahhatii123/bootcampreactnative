//No.1
//Looping Pertama
console.log("Looping Pertama");
var i = 2;

while(i<=20){
    console.log(i+" - I love coding");
    i=i+2;
}

console.log("\n");


//Looping Kedua
console.log("Looping Kedua");
var i = 20;
while(i>=2){
    console.log(i+" - I will become a mobile developer");
    i = i - 2;
}
console.log("\n");



//No.2
//Looping For
for(var x = 1;x<=20;x++){
    if(x%2!=0&&x%3==0){
        console.log(x+" - I Love Coding");
    }else if(x%2==0){
        console.log(x+" - Berkualitas");
    }else if(x%2!=0){
        console.log(x+" - Santai");
    }
}
console.log("\n");

//No.3
//Membuat Persegi Panjang # 8x4
for(var y=1;y<=4;y++){
    s = "";
    for(var z=1;z<=8;z++){
        s+= "#";
    }
    console.log(s);   
}
console.log("\n");


//No.4
//Tangga
for(var x=1;x<=7;x++){
    s = "";
    for(var y=1;y<=x;y++){
        s += "#";
    }
    console.log(s)
}
console.log("\n");


//No.5
//Membuat Papan Catur
for(var x=1;x<=8;x++){
    p = "";
    for(var y=1;y<=8;y++){
        if(x%2==0){
            if(y%2==0){
                p+=" ";
            }else{
                p+="#";
            }
        }else{
            if(y%2==0){
                p+="#";
            }else{
                p+=" ";
            }
        }
    }
    console.log(p);
}