//Nomor 1
//Release 0
class Animal{
    constructor(name, legs = 4, cold_blooded = false){
        this.name = name;
        this.legs = legs;
        this.cold_blooded = cold_blooded;
    }
}

var sheep = new Animal("shaun");

console.log(sheep.name);
console.log(sheep.legs);
console.log(sheep.cold_blooded);
console.log("<--><--><--><--><-->");

//Release 1
class Ape extends Animal{
    constructor(name, cold_blooded, legs = 2){
        super(name, cold_blooded);
        this.legs = legs;
    }
    yell(){
        console.log("Auuoooo");
    }
}

var sungokong = new Ape("Kera Sakti");
sungokong.yell()

class Frog extends Animal{
    constructor(name, legs,cold_blooded = true){
        super(name, legs);
        this.cold_blooded = cold_blooded;
    }
    jump(){
        console.log("hop hop");
    }
}

var kodok = new Frog("buduk");
kodok.jump();

console.log("<--><--><--><--><-->");

//nomor 2
class Clock {
  constructor({ template }) {
    this.template = template;
  }

  render() {
    let date = new Date();

    let hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    let mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    let secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;

    let output = this.template
      .replace('h', hours)
      .replace('m', mins)
      .replace('s', secs);

    console.log(output);
  }

  stop() {
    clearInterval(this.timer);
  }

  start() {
    this.render();
    this.timer = setInterval(() => this.render(), 1000);
  }
}


var clock = new Clock({template: 'h:m:s'});
clock.start();