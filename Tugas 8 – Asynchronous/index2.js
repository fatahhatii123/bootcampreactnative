var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]



function read(time, books, i){
    if(i < books.length){
        readBooksPromise(time,books[i])
            .then(function (fulfilled) {
                i+=1;
                read(fulfilled, books, i);
            })
            .catch(function (error) {
                console.log(error.message);
            });
    }
}
read(10000,books,0)


