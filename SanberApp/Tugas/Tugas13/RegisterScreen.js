import React from 'react';
import {Platform, View, Text, StyleSheet, Image, TouchableOpacity, FlatList, ViewPagerAndroidComponent, ImageStore, ColorPropType, TextInput, Button} from 'react-native';
import {Input, Item, Content} from 'native-base';


export default class App extends React.Component{

    render(){
        return(
            <View style={styles.container}>
                <View style={styles.logo}>
                    <Image source={require('./images/logo.png')} style={{width:280, height:90}} />
                </View>

                <View style={styles.form}>
                    <Text style={styles.title}>Login</Text>

                    <View style={styles.colom}>
                        <Text style={styles.colomtext}>Username</Text>
                        <TextInput style={styles.textinput}></TextInput>

                        <Text style={styles.colomtext}>Email</Text>
                        <TextInput style={styles.textinput}></TextInput>

                        <Text style={styles.colomtext}>Password</Text>
                        <TextInput style={styles.textinput}></TextInput>

                        <Text style={styles.colomtext}>Ulangi Password</Text>
                        <TextInput style={styles.textinput}></TextInput>
                    </View>
                </View>

                <View style={styles.buttonsec}>
                <TouchableOpacity
                    style={styles.roundButton1}>
                    <Text  style={{color: "#FFF", fontSize: 20}}>Daftar</Text>
                </TouchableOpacity>

                <Text style={{marginVertical: 12, fontSize: 17, color:"#003366" }}>Atau</Text>

                <TouchableOpacity
                    style={styles.roundButton2}>
                    <Text style={{color: "#3EC6FF", fontSize: 20}}>Masuk</Text>
                </TouchableOpacity>
                </View>
            </View>
            
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        marginTop: 20
    },
    form:{
        marginHorizontal:50,
    },
    colom: {
        marginTop: 35
    },
    textinput:{
        borderWidth:1.5,
        padding: 5,
        paddingLeft: 10,
        borderColor: "#003366",
        marginBottom: 5
    },
    logo: {
        alignItems: 'center',
        marginLeft:-15,
        marginTop: 40,
        justifyContent: 'center'
    },
    title:{
        marginTop: 70,
        color: "#003366",
        fontSize: 24,
        fontWeight: "bold",
        textAlign: 'center'
    },
    colomtext:{
        fontWeight: "bold",
        marginBottom: 5
    },
    buttonsec:{
        marginTop: -60,
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        position: "relative"
    },
    roundButton1: {
        width: 120,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 12,
        borderRadius: 10,
        backgroundColor: '#003366',
      },
      roundButton2: {
        width: 120,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 12,
        borderRadius: 10,
        backgroundColor: '#FFF',
        borderWidth: 1.5,
        borderColor: "#003366"
      },
})